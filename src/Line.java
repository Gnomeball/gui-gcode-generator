import static java.lang.Math.abs;
import static java.lang.Math.atan;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

class Line {

    private Point start;
    private Point end;
    private final int speed;

    Line(Point start, Point end, int speed) {
        this.start = start;
        this.end = end;
        this.speed = speed;
    }

    Point getStart() {
        return start;
    }

    Point getEnd() {
        return end;
    }

    double getLength() {
        return sqrt(
            pow(abs(this.end.getX() - this.start.getX()), 2) +
            pow(abs(this.end.getY() - this.start.getY()), 2));
    }

    public int getSpeed() {
        return speed;
    }

    double getAngle() {
        double a = abs(this.end.getY() - this.start.getY());
        double b = abs(this.end.getX() - this.start.getX());
        return atan(a/b);
    }

    void swapPoints() {
        Point temp = this.start;
        this.start = this.end;
        this.end = temp;
    }

    String printLine() {
        return
            "G90 G1 X" + start.getX() / 10 + " Y" + start.getY() / 10 + " F" + Controller.travelSpeed + "\n" +
            "M03 S" + Controller.laserPower + "\n" +
            "G90 G1 X" + end.getX() / 10 + " Y" + end.getY() / 10 + " F" + speed + "\n" +
            "M05" + "\n";
    }

}
