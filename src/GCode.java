import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

import static java.lang.Math.abs;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.Math.sqrt;

public class GCode {

    private static double totalPrintDistance;
    private static double totalTravelDistance;
    private static double totalTime;
    private static double efficiency;

//    private ArrayList<Line> buildSecondaryOutline(int[][] image) {
//        ArrayList<Point> points = new ArrayList<>();
//
//        for (int y = 1 ; y < image.length-1 ; y++) {
//            for (int x = 1 ; x < image[0].length-1 ; x++) {
//
//                if (image[y][x] == 1 && outlineGrid.isEdge(x, y)) {
//                    points.add(new Point(x, image.length - y));
//                }
//
//            }
//        }
//
//        ArrayList<Line> lines = new ArrayList<>();
//
//        for (int p = 0 ; p < points.size() ; p++) {
//            for (int q = p + 1 ; q < points.size() ; q++) {
//                Line temp = new Line(points.get(p), points.get(q));
//                if (temp.getLength() < 2) {
//                    lines.add(temp);
//                }
//            }
//        }
//
//        lines = orderLines(lines);
//
//        for (int i = 1 ; i < lines.size() ; i++) {
//            if (lines.get(i-1).getAngle() == lines.get(i).getAngle() && lines.get(i-1).getEnd() == lines.get(i).getStart()) {
//                Line temp = new Line(lines.get(i-1).getStart(), lines.get(i).getEnd());
//                lines.set(i-1, temp);
//                lines.remove(i);
//                i--;
//            }
//        }
//
//        return lines;
//    }
//
//    private void buildOutlineGrid(int[][] edgePoints) {
//
//        for (int x = 0 ; x < edgePoints[0].length ; x++) {
//            for (int y = 0 ; y < edgePoints.length ; y++) {
//
//                if (edgePoints[y][x] == 1) {
//                    outlineGrid.setCell(x-1, y-1, 1);
//                    outlineGrid.setCell(x-1, y, 1);
//                    outlineGrid.setCell(x-1, y+1, 1);
//
//                    outlineGrid.setCell(x, y-1, 1);
//                    outlineGrid.setCell(x, y, 1);
//                    outlineGrid.setCell(x, y+1, 1);
//
//                    outlineGrid.setCell(x+1, y-1, 1);
//                    outlineGrid.setCell(x+1, y, 1);
//                    outlineGrid.setCell(x+1, y+1, 1);
//                }
//
//            }
//        }
//
//    }

    private Grid fullImageGrid;
    private Grid edgePointGrid;
    private Grid infillGrid;

    public void generateGCode(BufferedImage bufferedImage, String state) {
        switch (state) {
            case "Horizontal":
                generateHorizontal(bufferedImage);
                break;
            case "Diagonal":
                generateDiagonal(bufferedImage);
                break;
            default:
                generateOutline(bufferedImage);
                break;
        }
    }

    private void generateOutline(BufferedImage bufferedImage) {
        resetVariables();
        buildArrays(bufferedImage);

        ArrayList<Line> lines = buildOutline(edgePointGrid);

        try {
            writeGCodeToFile(lines);
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }
    }

    private void generateHorizontal(BufferedImage bufferedImage) {
        resetVariables();
        buildArrays(bufferedImage);
        ArrayList<Line> outline = buildOutline(edgePointGrid);
        ArrayList<Line> infill = buildHorizontalInfill(infillGrid);

        ArrayList<Line> lines = new ArrayList<>(outline);
        lines.addAll(infill);

        try {
            writeGCodeToFile(lines);
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }
    }

    private void generateDiagonal(BufferedImage bufferedImage) {
        resetVariables();
        buildArrays(bufferedImage);
        ArrayList<Line> outline = buildOutline(edgePointGrid);
        ArrayList<Line> infill = buildDiagonalInfill(infillGrid);

        ArrayList<Line> lines = new ArrayList<>(outline);
        lines.addAll(infill);

        try {
            writeGCodeToFile(lines);
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }
    }

    private void writeGCodeToFile(ArrayList<Line> lines) throws FileNotFoundException {

        PrintWriter out = new PrintWriter(new File(Controller.filename + ".gcode"));

        out.println("; Not So Terrible GCode Generator Mk6");
        out.println();
        out.println("; File : " + Controller.filename);
        out.println();
        out.println("; Outline Speed\t: " + Controller.outlineSpeed);
        out.println("; Infill Speed\t: " + Controller.infillSpeed);
        out.println("; Travel Speed\t: " + Controller.travelSpeed);
        out.println("; Laser Power\t: " + Controller.laserPower);
        out.println("; Lines per mm\t: " + Controller.lineDensity);
        out.println();
        out.println("; Total Print Distance\t: " + round(totalPrintDistance) / 10 + " mm");
        out.println("; Total Travel Distance\t: " + round(totalTravelDistance) / 10 + " mm");
        out.println();
        out.println("; Estimated Total Time\t: " + round(totalTime) / 10 + " minutes");
        out.println();
        out.println("; Print Efficiency\t: " + round(efficiency) + " %");
        out.println();
        out.println("; ABSOLUTE POSITIONING");
        out.println("G90");
        out.println();
        out.println("; START IMAGE");
        out.println();

        for (Line l : lines) {
            out.print(l.printLine());
        }

        out.println();
        out.println("; END IMAGE");
        out.println();
        out.println("G28");

        out.close();
    }

    private void buildArrays(BufferedImage bufferedImage) {
        fullImageGrid = buildImageArray(bufferedImage);
        edgePointGrid = buildEdgePointArray(fullImageGrid);
        infillGrid = buildInfillArray(fullImageGrid);
    }

    private Grid buildImageArray(BufferedImage bufferedImage) {
        Grid temp = new Grid(bufferedImage.getWidth(), bufferedImage.getHeight());
        for (int x = 0 ; x < bufferedImage.getWidth() ; x++) {
            for (int y = 0 ; y < bufferedImage.getHeight() ; y++) {
                temp.setCell(x, y, bufferedImage.getRGB(x, y) == 0 ? 0 : 1);
            }
        }
        return temp;
    }

    private Grid buildEdgePointArray(Grid fullImageGrid) {
        int[][] grid = fullImageGrid.getGrid();
        Grid temp = new Grid(grid[0].length, grid.length);
        for (int y = 1 ; y < grid.length-1 ; y++) {
            for (int x = 1 ; x < grid[0].length-1 ; x++) {
                if (grid[y][x] == 1 && fullImageGrid.isEdge(x, y)) {
                    temp.setCell(x, y, 1);
                }
            }
        }
        return temp;
    }

    private Grid buildInfillArray(Grid fullImageGrid) {
        int[][] grid = fullImageGrid.getGrid();
        Grid temp = new Grid(grid[0].length, grid.length);
        for (int y = 1 ; y < grid.length-1 ; y++) {
            for (int x = 1 ; x < grid[0].length-1 ; x++) {
                if (grid[y][x] == 1 && !fullImageGrid.isEdge(x, y)) {
                    temp.setCell(x, y, 1);
                }
            }
        }
        return temp;
    }

    private ArrayList<Point> buildEdgePoints(Grid edgePointGrid) {
        int[][] grid = edgePointGrid.getGrid();
        ArrayList<Point> points = new ArrayList<>();
        for (int y = 1 ; y < grid.length-1 ; y++) {
            for (int x = 1 ; x < grid[0].length-1 ; x++) {
                if (grid[y][x] == 1) {
                    points.add(new Point(x, grid.length - y));
                }
            }
        }
        return points;
    }

    private ArrayList<Line> buildOutline(Grid edgePointGrid) {
        ArrayList<Point> points = buildEdgePoints(edgePointGrid);
        ArrayList<Line> lines = new ArrayList<>();
        for (int p = 0 ; p < points.size() ; p++) {
            for (int q = p + 1 ; q < points.size() ; q++) {
                Line temp = new Line(points.get(p), points.get(q), Controller.outlineSpeed);
                if (temp.getLength() < 2) {
                    lines.add(temp);
                }
            }
        }
        lines = orderLines(lines);
        for (int i = 1 ; i < lines.size() ; i++) {
            if (lines.get(i-1).getAngle() == lines.get(i).getAngle() && lines.get(i-1).getEnd() == lines.get(i).getStart()) {
                Line temp = new Line(lines.get(i-1).getStart(), lines.get(i).getEnd(), Controller.outlineSpeed);
                lines.set(i-1, temp);
                lines.remove(i);
                i--;
            }
        }
        return lines;
    }

    private ArrayList<Line> buildDiagonalInfill(Grid infillGrid) {
        int[][] grid = infillGrid.getGrid();

        ArrayList<Line> lines = new ArrayList<>();

        Point tempStart = new Point(0, 0);
        Point tempEnd;

        int previous_pixel = 0;
        int density = 10 / Controller.lineDensity;

        for (int z = 0 ; z < grid.length ; z += density) {
            for (int x = 0, y = z ; y < grid.length ; x++, y++) {

                int pixel = grid[y][x];
                int y_a = grid.length - y;

                if (pixel != previous_pixel) {

                    if (pixel == 1) { // START
                        tempStart = new Point(x, y_a);
                    }

                    if (pixel == 0) { // END
                        tempEnd = new Point(x, y_a);
                        lines.add(new Line(tempStart, tempEnd, Controller.infillSpeed));
                    }

                }

                previous_pixel = pixel;

            }
        }

        tempStart = new Point(0, 0);
        previous_pixel = 0;

        for (int z = density ; z < grid.length ; z += density) {
            for (int x = z, y = 0 ; x < grid.length ; x++, y++) {

                int pixel = grid[y][x];
                int y_a = grid.length - y;

                if (pixel != previous_pixel) {

                    if (pixel == 1) { // START
                        tempStart = new Point(x, y_a);
                    }

                    if (pixel == 0) { // END
                        tempEnd = new Point(x, y_a);
                        lines.add(new Line(tempStart, tempEnd, Controller.infillSpeed));
                    }

                }

                previous_pixel = pixel;

            }
        }

        lines = orderLines(lines);

        return lines;

    }

    private ArrayList<Line> buildHorizontalInfill(Grid infillGrid) {
        int[][] grid = infillGrid.getGrid();

        ArrayList<Line> lines = new ArrayList<>();

        Point tempStart = new Point(0, 0);
        Point tempEnd;

        int previous_pixel = 0;
        int density = 10 / Controller.lineDensity;

        for (int y = 0 ; y < grid.length ; y += density) {
            for (int x = 0 ; x < grid[0].length ; x++) {

                int pixel = grid[y][x];
                int y_a = grid.length - y;

                if (pixel != previous_pixel) {

                    if (pixel == 1) { // START
                        tempStart = new Point(x, y_a);
                    }

                    if (pixel == 0) { // END
                        tempEnd = new Point(x, y_a);
                        lines.add(new Line(tempStart, tempEnd, Controller.infillSpeed));
                    }

                }

                previous_pixel = pixel;

            }
        }

        lines = orderLines(lines);

        return lines;
    }

    private ArrayList<Line> orderLines(ArrayList<Line> lines) {
        ArrayList<Line> temp = new ArrayList<>();

        Line previous;
        double start;
        double end;
        double distanceFromPrevious;
        double minimumTravelDistance = Integer.MAX_VALUE;
        Line closest = null;

        temp.add(new Line(new Point(0, 0), new Point(0, 0), 0));
        Collections.reverse(lines);

        while (lines.size() > 0) {

            Line next = null;

            for (Line line : lines) {
                next = line;
                previous = temp.get(temp.size() - 1);

                start = sqrt(
                    pow(abs((line.getStart().getX() - previous.getEnd().getX())), 2) +
                        pow(abs((line.getStart().getY() - previous.getEnd().getY())), 2));

                end = sqrt(
                    pow(abs((line.getEnd().getX() - previous.getEnd().getX())), 2) +
                        pow(abs((line.getEnd().getY() - previous.getEnd().getY())), 2));

                if (end < start) {
                    line.swapPoints();
                }

                distanceFromPrevious = min(start, end);

                if (distanceFromPrevious < minimumTravelDistance) {
                    closest = line;
                    minimumTravelDistance = distanceFromPrevious;
                }
            }

            // Add travel distance and time
            totalTravelDistance += minimumTravelDistance;
            totalTime += minimumTravelDistance / Controller.travelSpeed;

            // Add line distance and time
            assert closest != null;
            double length = closest.getLength();
            totalPrintDistance += length;
            totalTime += length / next.getSpeed();

            // Transfer line to output
            temp.add(lines.get(lines.indexOf(closest)));
            lines.remove(closest);

            // Reset max distance
            minimumTravelDistance = Integer.MAX_VALUE;

        }

        temp.remove(0);

        // Calculate efficiency
        efficiency = totalPrintDistance / (totalPrintDistance + totalTravelDistance) * 100;

        return temp;
    }

    private void resetVariables() {
        totalPrintDistance = 0;
        totalTravelDistance = 0;
        totalTime = 0;
        efficiency = 0;
    }

    private void logArrayToFile(Grid grid) {
        int[][] image = grid.getGrid();

        PrintWriter pw;

        try {
            pw = new PrintWriter(new File("log.txt"));
            pw.flush();

            for (int y = 0 ; y < image.length ; y += 2) {
                for (int x = 0 ; x < image[0].length ; x++) {
                    pw.print(image[y][x]);
                }
                pw.println();
            }

            pw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
