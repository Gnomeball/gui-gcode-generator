public class Grid {

    private final int[][] grid;

    public Grid(int x, int y) {
        grid = new int[y][x];
        initialise();
    }

    private void initialise() {
        for (int y = 0 ; y < this.getGrid().length ; y++) {
            for (int x = 0 ; x < this.getGrid()[0].length ; x++) {
                this.setCell(x, y, 0);
            }
        }
    }

    public int[][] getGrid() {
        return grid;
    }

    public void setCell(int x, int y, int i) {
        grid[y][x] = i;
    }

    public boolean isEdge(int x, int y) {
        for (int i : getNeighbours(x, y)) {
            if (i == 0) {
                return true;
            }
        }
        return false;
    }

    private int[] getNeighbours(int x, int y) throws IndexOutOfBoundsException {
        if (0 == x || grid[0].length == x) {
            throw new IndexOutOfBoundsException("X out of bounds.");
        } else if (0 == y || grid.length == y) {
            throw new IndexOutOfBoundsException("Y out of bounds.");
        } else {
            return new int[] {
                grid[y+1][x],
                grid[y][x+1],
                grid[y-1][x],
                grid[y][x-1],
            };
        }
    }

}
