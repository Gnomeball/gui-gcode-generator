import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Stylesheet.fxml"));

        stage.setScene(new Scene(root));
        stage.setTitle("CNC Laser GCode Generator");
        stage.setResizable(false);
        stage.show();
    }

}
