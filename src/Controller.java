import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Controller {

    public static String filename;

    public static int outlineSpeed;
    public static int infillSpeed;
    public static int travelSpeed;
    public static int laserPower;
    public static int lineDensity;

    public static BufferedImage loadedImage = null;

    final GCode gcode = new GCode();

    public ImageView image;
    public Slider outlineSlider;
    public Slider infillSlider;
    public Slider travelSlider;
    public Slider powerSlider;
    public Slider densitySlider;
    public Label outlineLabel;
    public Label infillLabel;
    public Label travelLabel;
    public Label powerLabel;
    public Label densityLabel;
    public TextField fileSelectTextBox;
    public Button fileSelectionButton;
    public RadioButton outlineRadioButton;
    public RadioButton horizontalRadioButton;
    public RadioButton diagonalRadioButton;
    public Button sliceButton;

    public void initialize() {

        setVariables();

        Slider[] sliders = new Slider[] {
            outlineSlider, infillSlider, travelSlider, powerSlider, densitySlider
        };

        int[] ints = new int[] {
            outlineSpeed, infillSpeed, travelSpeed, laserPower, lineDensity
        };

        Label[] labels = new Label[] {
            outlineLabel, infillLabel, travelLabel, powerLabel, densityLabel
        };

        for (int i = 0 ; i < sliders.length ; i++) {
            int finalI = i;
            sliders[i].valueProperty().addListener(((observable, oldValue, newValue) -> {
                ints[finalI] = newValue.intValue();
                labels[finalI].setText(Integer.toString(ints[finalI]));
            }));
        }

        fileSelectionButton.setOnMouseClicked(event -> {
            try {
                filename = fileSelectTextBox.getText();
                loadedImage = ImageIO.read(new File("./img/" + filename + ".png"));
                image.setImage(SwingFXUtils.toFXImage(loadedImage, null));
            } catch (IOException e) {
                System.out.println("File not found.");
            }
        });

        ToggleGroup tg = new ToggleGroup();
        outlineRadioButton.setToggleGroup(tg);
        horizontalRadioButton.setToggleGroup(tg);
        diagonalRadioButton.setToggleGroup(tg);

        outlineRadioButton.fire();

        sliceButton.setOnMouseClicked(event -> {
            setVariables();
            String state;
            if (horizontalRadioButton.isSelected()) {
                state = "Horizontal";
            } else if (diagonalRadioButton.isSelected()) {
                state = "Diagonal";
            } else {
                state = "Outline";
            }
            gcode.generateGCode(loadedImage, state);
        });

    }

    private void setVariables() {
        outlineSpeed = outlineSlider.valueProperty().intValue();
        infillSpeed = infillSlider.valueProperty().intValue();
        travelSpeed = travelSlider.valueProperty().intValue();
        laserPower = powerSlider.valueProperty().intValue();
        lineDensity = densitySlider.valueProperty().intValue();

        outlineLabel.setText(String.valueOf(outlineSpeed));
        infillLabel.setText(String.valueOf(infillSpeed));
        travelLabel.setText(Integer.toString(travelSpeed));
        powerLabel.setText(Integer.toString(laserPower));
        densityLabel.setText(Integer.toString(lineDensity));
    }

}
