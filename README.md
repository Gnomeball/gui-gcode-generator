# NotSoTerribleGCodeGenerator

An updated version to my original GCode Generator, this time with an equally terrible GUI.

Also included is an outline only mode, differing speeds for outline and infill and both horizontal and diagonal infill.

Note that sometimes diagonal infill may not work, I'm planning to fix it.. eventually.

![GUI](Gui.png)

![Example Output](Example.png)